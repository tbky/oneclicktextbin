lParameters cSelectedFolder

_vfp.Visible=.f.

IF EMPTY(cSelectedFolder)
	cSelectedFolder = GETDIR()	
ELSE
	cSelectedFolder = ALLTRIM(UPPER(cSelectedFolder))+'\'
ENDIF

WAIT "Please wait while your files are generated." WINDOW NOWAIT NOCLEAR 
LOCAL cSccTextApp, filecount, nrecs, cfile, nfiles, i
nfiles=0

XMLTOCURSOR("..\exe\config.txt",'config',512)
cSccTextApp	= config.path_to_prg
LOCAL loCnv AS c_foxbin2prg OF cSccTextApp
loCnv = NEWOBJECT("c_foxbin2prg", cSccTextApp)


Do Declare

Local loDir
loDir = Createobject("Tdir", cSelectedFolder)
Go Top
SCAN
	IF filecount>0 
		i = 1
		nrecs=0
		local array aFileArray[1,5]
		nrecs=ADIR(aFileArray,ALLTRIM(Fullname)+'*.*')
		FOR i = 1 TO nrecs
			cfile=ALLTRIM(Fullname)+aFileArray[i,1]
			IF INLIST(ALLTRIM(UPPER(JUSTEXT(cfile))),'PJ2','SC2','VC2','FR2','LB2','DC2','DB2')
				loCnv.Ejecutar( cfile,null,null,null,1,null,1 )
				nfiles=nfiles+1
			ENDIF 
		NEXT i		
	ENDIF 
ENDSCAN

WAIT CLEAR 
MESSAGEBOX(TRANSFORM(nfiles)+' files converted')

CANCEL 

Define Class Tdir As Custom
	cursorname = ""
	treelevel  = 0
	root	   = ""
	recordid   = 0
	parentid   = -1
	maxid	   = 0

	Procedure Init(lcPath, loParent)
	If Type("loParent") = "O"
		This.cursorname	= loParent.cursorname
		This.root		= loParent.root
		This.root.maxid	= This.root.maxid + 1
		This.recordid	= This.root.maxid
		This.parentid	= loParent.recordid
		This.treelevel	= loParent.treelevel + 1
	Else
		This.root		= This
		This.cursorname	= "cs" + Substr(Sys(2015), 3, 10)

		Select 0
		Create Cursor (This.cursorname) (recordid N(6), parentid N(6), ;
			  treelevel N(3), dircount N(6), filecount N(6), ;
			  filesize N(12), utclastwrite T, FullName C(250))
	Endif

	Insert Into (This.cursorname) Values (This.recordid, ;
		  This.parentid, This.treelevel, 0, 0, 0, {}, lcPath)

	This.DoFind(lcPath)

	Procedure DoFind(lcPath)
	#Define MAX_PATH 260
	#Define FILE_ATTRIBUTE_DIRECTORY 16
	#Define INVALID_HANDLE_VALUE -1
	#Define MAX_DWORD 0xffffffff+1

*| typedef struct _WIN32_FIND_DATA {
*|   DWORD    dwFileAttributes;           0:4
*|   FILETIME ftCreationTime;             4:8
*|   FILETIME ftLastAccessTime;          12:8
*|   FILETIME ftLastWriteTime;           20:8
*|   DWORD    nFileSizeHigh;             28:4
*|   DWORD    nFileSizeLow;              32:4
*|   DWORD    dwReserved0;               36:4
*|   DWORD    dwReserved1;               40:4
*|   TCHAR    cFileName[ MAX_PATH ];     44:260
*|   TCHAR    cAlternateFileName[ 14 ]; 304:14
*| } WIN32_FIND_DATA, *PWIN32_FIND_DATA; total bytes = 318
	#Define FIND_DATA_SIZE  318

	lcPath = Alltrim(lcPath)
	If Right(lcPath, 1) <> "\"
		lcPath = lcPath + "\"
	Endif

	Local hFind, cFindBuffer, lnAttr, cFilename, nFileCount, ;
		nDirCount, nFileSize, cWriteTime, nLatestWriteTime, oNext

	cFindBuffer	= Repli(Chr(0), FIND_DATA_SIZE)
	hFind		= FindFirstFile(lcPath + "*.*", @cFindBuffer)
	If hFind = INVALID_HANDLE_VALUE
		Return
	Endif

	Store 0 To nDirCount, nFileCount, nFileSize, nLatestWriteTime
	Do While .T.
		lnAttr	  = buf2dword(Substr(cFindBuffer, 1, 4))
		cFilename = Substr(cFindBuffer, 45, MAX_PATH)
		cFilename = Left(cFilename, At(Chr(0), cFilename) - 1)

		cWriteTime = Substr(cFindBuffer, 21, 8)
		If Empty(nLatestWriteTime)
			nLatestWriteTime = cWriteTime
		Else
			If CompareFileTime(cWriteTime, nLatestWriteTime) = 1
				nLatestWriteTime = cWriteTime
			Endif
		Endif

		If Bitand(lnAttr, FILE_ATTRIBUTE_DIRECTORY) = FILE_ATTRIBUTE_DIRECTORY
* for a directory
			nDirCount = nDirCount + 1
			If Not Left(cFilename, 1) = "."
				oNext = Createobject("Tdir", lcPath + cFilename + "\", This)
			Endif
		Else
* for a regular file
			nFileCount = nFileCount + 1
			nFileSize = nFileSize + ;
				buf2dword(Substr(cFindBuffer, 29, 4)) * MAX_DWORD + ;
				buf2dword(Substr(cFindBuffer, 33, 4))
		Endif

		If FindNextFile(hFind, @cFindBuffer) = 0
			Update (This.cursorname) Set dircount = m.nDirCount, ;
				filecount = m.nFileCount, filesize = m.nFileSize, ;
				utclastwrite = sys2dt(nLatestWriteTime);
				Where recordid = This.recordid
			Exit
		Endif
	Enddo
	= FindClose(hFind)
Enddefine

Function sys2dt(lcFiletime)
* converts a SYSTEMTIME buffer to the DATETIME value
Local lcSystime, lcStoredSet, lcDate, lcTime, ltResult, ;
	lnYear, lnMonth, lnDay, lnHours, lnMinutes, lnSeconds

lcSystime = Repli(Chr(0), 16)
= FileTimeToSystemTime(lcFiletime, @lcSystime)

lnYear	  = buf2word(Substr(lcSystime, 1, 2))
lnMonth	  = buf2word(Substr(lcSystime, 3, 2))
lnDay	  = buf2word(Substr(lcSystime, 7, 2))
lnHours	  = buf2word(Substr(lcSystime, 9, 2))
lnMinutes = buf2word(Substr(lcSystime, 11, 2))
lnSeconds = buf2word(Substr(lcSystime, 13, 2))

lcStoredSet = Set("DATE")
Set Date To Mdy

lcDate = Strtran(Str(lnMonth, 2) + "/" + Str(lnDay, 2) + ;
	  "/" + Str(lnYear, 4), " ", "0")

lcTime = Strtran(Str(lnHours, 2) + ":" + Str(lnMinutes, 2) + ;
	  ":" + Str(lnSeconds, 2), " ", "0")

ltResult = Ctot(lcDate + " " + lcTime)
Set Date To &lcStoredSet
Return  ltResult

Function buf2word(lcBuffer)
Return Asc(Substr(lcBuffer, 1, 1)) + ;
	Asc(Substr(lcBuffer, 2, 1)) * 256

Function buf2dword(lcBuffer)
Return Asc(Substr(lcBuffer, 1, 1)) + ;
	Asc(Substr(lcBuffer, 2, 1)) * 256 + ;
	Asc(Substr(lcBuffer, 3, 1)) * 65536 + ;
	Asc(Substr(lcBuffer, 4, 1)) * 16777216

Procedure Declare
Declare Integer FindClose In kernel32 Integer hFindFile

Declare Integer FindFirstFile In kernel32;
	String lpFileName, String @lpFindFileData

Declare Integer FindNextFile In kernel32;
	Integer hFindFile, String @lpFindFileData

Declare Integer FileTimeToSystemTime In kernel32;
	String lpFileTime, String @lpSystemTime

Declare Integer CompareFileTime In kernel32;
	String lpFileTime1, String lpFileTime2
	
	